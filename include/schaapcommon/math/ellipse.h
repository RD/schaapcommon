#ifndef SCHAAPCOMMON_MATH_ELLIPSE_H_
#define SCHAAPCOMMON_MATH_ELLIPSE_H_

namespace schaapcommon::math {

struct Ellipse {
  double major;
  double minor;
  double position_angle;
};

}  // namespace schaapcommon::math

#endif
